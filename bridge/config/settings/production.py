# -*- coding: utf-8 -*-
from .base import *


DEBUG = False

ALLOWED_HOSTS = ['*']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'bridgenews',
        'USER': 'bridge',
        'PASSWORD': 'ev01uk',
        'HOST': 'localhost',
        'PORT': '',
    }
}

STATIC_ROOT = '/webapps/bridge_django/bridge_project/static'
MEDIA_ROOT = '/webapps/bridge_django/bridge_project/media'