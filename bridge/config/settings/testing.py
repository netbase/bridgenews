# -*- coding: utf-8 -*-
from .base import *


DEBUG = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'bridge_db',
        'USER': 'bridge',
        'PASSWORD': 'ev01uk',
        'HOST': '',
        'PORT': '',
    }
}