# -*- coding: utf-8 -*-
from django.test import TestCase
from django.core.urlresolvers import reverse


class TestHomePage(TestCase):

    def test_uses_index_template(self):
        response = self.client.get(reverse("home"))
        self.assertTemplateUsed(response, "pages/home.html")

    def test_uses_base_template(self):
        response = self.client.get(reverse("home"))
        self.assertTemplateUsed(response, "base.html")


class TestHomeFiles(TestCase):

    def test_robots_file(self):
        response = self.client.get("/robots.txt")
        self.assertTemplateUsed(response, "robots.txt")

    def test_humans_file(self):
        response = self.client.get("/humans.txt")
        self.assertTemplateUsed(response, "humans.txt")