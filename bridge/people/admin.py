# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import Author


class AuthorAdmin(admin.ModelAdmin):
    raw_id_fields = ('user', )


admin.site.register(Author, AuthorAdmin)