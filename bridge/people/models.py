# -*- coding: utf-8 -*-
from django.db import models
from django.conf import settings
from django.utils.translation import ugettext_lazy as _

from . import managers


class Author(models.Model):
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL,
        related_name="author",
        verbose_name=_("Χρήστης")
        )
    bio = models.TextField(
        verbose_name=_("Βιογραφικό"),
        blank=True,
    )

    objects = managers.AuthorManager

    @property
    def name(self):
        return ("%s %s" % (self.user.first_name.strip(), self.user.last_name.strip())).strip() or self.user.username

    @property
    def email(self):
        return self.user.email

    class Meta:
        verbose_name = _("Αρθρογράφος")
        verbose_name_plural = _("Αρθρογράφοι")
        ordering = ('user',)

    def __str__(self):
        return self.user.username