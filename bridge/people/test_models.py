# -*- coding: utf-8 -*-
from django.test import TestCase

from model_mommy import mommy

from .models import Author


class AuthorTestCase(TestCase):
    def setUp(self):
         self.author = mommy.make('Author', bio="YOLO", user__first_name="Nikos", user__last_name="Delimpaltadakis",
                                  user__email="delinikos@gmail.com", user__username="delibaltas")
         self.user = self.author.user
         self.author1 = mommy.make('Author', bio="YOLO", user__first_name=" Nikos  ", user__last_name="Delimpaltadakis  ",
                                   user__username="delibaltas1")
         self.author2 = mommy.make('Author', bio="YOLO", user__first_name="", user__last_name="Delimpaltadakis",
                                   user__username="delibaltas2")
         self.author3 = mommy.make('Author', bio="YOLO", user__first_name="Nikos", user__last_name="",
                                   user__username="delibaltas3")
         self.author4 = mommy.make('Author', bio="YOLO", user__first_name="", user__last_name="",
                                   user__username="delibaltas4")

    def test_author_creation(self):
        author = Author.objects.get(user=self.user)
        self.assertEqual(author.bio, "YOLO")

    def test_author_str(self):
        self.assertEqual(str(self.author), "delibaltas")

    def test_author_name(self):
        self.assertEqual(self.author.name, "Nikos Delimpaltadakis")
        self.assertEqual(self.author1.name, "Nikos Delimpaltadakis")
        self.assertEqual(self.author2.name, "Delimpaltadakis")
        self.assertEqual(self.author3.name, "Nikos")
        self.assertEqual(self.author4.name, "delibaltas4")

    def test_author_email(self):
        self.assertEqual(self.author.email, "delinikos@gmail.com")
