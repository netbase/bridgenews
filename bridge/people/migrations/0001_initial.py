# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Author',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('bio', models.TextField(verbose_name='Βιογραφικό', blank=True)),
                ('user', models.OneToOneField(verbose_name='Χρήστης', related_name='author', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ('user',),
                'verbose_name': 'Αρθρογράφος',
                'verbose_name_plural': 'Αρθρογράφοι',
            },
        ),
    ]
