# -*- coding: utf-8 -*-
from django.conf.urls import url

from .views import post_detail, posts_index

urlpatterns = [
    url(r'^$', posts_index, name='posts-index'),
    url(r'^detail/(?P<year>\d{4})/(?P<month>\w{3})/(?P<day>\d{2})/(?P<slug>[-\w]+)/$', post_detail, name='post-detail'),
]