# -*- coding: utf-8 -*-

from markdown import markdown

from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.utils.timezone import now
from django.core.urlresolvers import reverse

from people.models import Author
from . import managers


class Category(models.Model):
    title = models.CharField(verbose_name=_("Τίτλος"), max_length=250)
    slug = models.SlugField(unique=True)
    description = models.TextField(blank=True)

    objects = managers.CategoryManager

    class Meta:
        verbose_name = _("Κατηγορία")
        verbose_name_plural = _("Κατηγορίες")
        ordering = ('title',)

    def __str__(self):
        return self.title


class Post(models.Model):
    title = models.CharField(verbose_name=_("Τίτλος"), max_length=250)
    teaser = models.TextField(verbose_name=_("Εισαγωγή"), blank=True)
    body = models.TextField(verbose_name=_("Κείμενο"))
    pub_date = models.DateTimeField(verbose_name=_("Δημοσίευση"), default=now)

    author = models.ForeignKey(Author, verbose_name=_("Συγγραφέας"))
    categories = models.ManyToManyField(Category, verbose_name=_("Κατηγορίες"), blank=True)
    slug = models.SlugField(unique_for_date='pub_date')

    objects = managers.PostManager

    class Meta:
        verbose_name = _("Δημοσίευση")
        verbose_name_plural = _("Δημοσιεύσεις")
        ordering = ['-pub_date']

    def html(self):
        return markdown(self.body)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        date = self.pub_date.strftime("%Y/%b/%d")
        d = date.split('/')
        return reverse('post-detail', kwargs={'year': d[0], 'month': d[1], 'day': d[2], 'slug': self.slug})
