# -*- coding: utf-8 -*-
import datetime, time

from django.shortcuts import render, get_object_or_404

from .models import Post


def posts_index(request):
    posts = Post.objects.all()

    return render(request, "posts/posts_index.html", { 'posts': posts })


def post_detail(request, year, month, day, slug):
    date_stamp = time.strptime(year+month+day, "%Y%b%d")
    pub_date = datetime.date(*date_stamp[:3])
    post = get_object_or_404(Post,
                             pub_date__year=pub_date.year,
                             pub_date__month=pub_date.month,
                             pub_date__day=pub_date.day,
                             slug=slug)
    return render(request, "posts/post_detail.html", { 'post': post })
