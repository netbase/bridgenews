# -*- coding: utf-8 -*-
from django.test import TestCase

from model_mommy import mommy

from .models import Category


class CategoryTestCase(TestCase):
    def setUp(self):
        self.category = mommy.make('Category', title="A Title", slug="a-title", description="A description")
        self.category_max = mommy.make('Category', _fill_optional=True)

    def test_category_creation(self):
        self.assertEqual(self.category.title, "A Title")
        self.assertIsNotNone(self.category_max.id)

    def test_category_str(self):
        self.assertEqual(str(self.category), "A Title")


class PostTestCase(TestCase):
    def setUp(self):
        self.post = mommy.make('Post', title="A Title", slug="a-title", body="A description\n\nAnd a new line.",  _fill_optional=True)
        self.post_max = mommy.make('Post', _fill_optional=True)

    def test_post_creation(self):
        self.assertEqual(self.post.title, "A Title")
        self.assertIsNotNone(self.post_max.id)

    def test_post_str(self):
        self.assertEqual(str(self.post), "A Title")

    def test_post_html(self):
        self.assertEqual((self.post.html()), "<p>A description</p>\n<p>And a new line.</p>")