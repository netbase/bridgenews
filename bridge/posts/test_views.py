# -*- coding: utf-8 -*-
import datetime
import pytz

from django.test import TestCase
from django.core.urlresolvers import reverse

from model_mommy import mommy

from .models import Category


class PostsTestCase(TestCase):
    def setUp(self):
        mommy.make('Post', title="A Title", slug="a-title", body="A description")
        mommy.make('Post', title="A second Title", slug="a-title-more", body="A description more")
        self.response = self.client.get(reverse("posts-index"))

    def test_posts_index_status(self):
        self.assertEqual(self.response.status_code, 200)

    def test_posts_index_template(self):
        self.assertTemplateUsed(self.response, "posts/posts_index.html")

    def test_posts_index_content(self):
        posts = self.response.context['posts']
        self.assertEqual(len(posts), 2)
        self.assertEqual(posts[1].title, "A Title")


class PostDetailTestCase(TestCase):
    def setUp(self):
        post = mommy.make('Post', title="A Title", slug="a-title", body="A description", pub_date=datetime.datetime(2016, 2, 8, 0, 2, 14, 0, pytz.timezone('EET')))
        self.response = self.client.get(post.get_absolute_url())

    def test_post_detail_status(self):
        self.assertEqual(self.response.status_code, 200)

    def test_post_detail_template(self):
        self.assertTemplateUsed(self.response, "posts/post_detail.html")

    def test_posts_index_content(self):
        post = self.response.context['post']
        self.assertEqual(post.title, "A Title")
