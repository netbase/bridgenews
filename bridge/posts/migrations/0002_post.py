# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('people', '0001_initial'),
        ('posts', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Post',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('title', models.CharField(verbose_name='Τίτλος', max_length=250)),
                ('teaser', models.TextField(verbose_name='Εισαγωγή', blank=True)),
                ('body', models.TextField(verbose_name='Κείμενο')),
                ('pub_date', models.DateTimeField(verbose_name='Δημοσίευση', default='django.utils.timezone.now')),
                ('slug', models.SlugField(unique_for_date='pub_date')),
                ('author', models.ForeignKey(verbose_name='Συγγραφέας', to='people.Author')),
                ('categories', models.ManyToManyField(verbose_name='Κατηγορίες', to='posts.Category', blank=True)),
            ],
            options={
                'verbose_name': 'Δημοσίευση',
                'ordering': ['-pub_date'],
                'verbose_name_plural': 'Δημοσιεύσεις',
            },
        ),
    ]
